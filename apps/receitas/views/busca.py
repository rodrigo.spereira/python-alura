from django.shortcuts import get_object_or_404, render
from receitas.models import Receita

def buscar(request):
    lista_receitas = Receita.objects.order_by('-date_receita').filter(publicado=True)

    if 'search' in request.GET:
        query_busca = request.GET['search']
        lista_receitas = lista_receitas.filter(nome_receita__icontains=query_busca)
    
    dados = {
        'receitas': lista_receitas
    }
    return render(request, "receitas/buscar.html", dados)